﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManagerScript : MonoBehaviour
{

	public static AudioManagerScript instance;
	private AudioSource audioSource;

	public void Awake()
	{

		if (instance == null)
			instance = this;

	}

	public void PlaySound(AudioClip clip)
	{

		audioSource = GetComponent<AudioSource>();
		audioSource.clip = clip;

		audioSource.volume = 0.5f;
		audioSource.Play();

	}

	// Start is called before the first frame update
	void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
