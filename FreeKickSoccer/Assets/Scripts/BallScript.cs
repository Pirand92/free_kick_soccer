﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScript : MonoBehaviour
{

	Vector2 startPos; // Position finger at start
	Vector2 endPos; // Position finger at finish
	Vector2 direction; // Direction of finger on display

	/* Calculate the force at ball */
	float touchTimeStart;  // Instant that finger touch the display
	float touchTimeFinish;  // Instant that finger doesn't touch more the display
	float timeInterval; // Differences obtained by time start and finish

	bool flag;

	[SerializeField]
	float forceInXandY = 1f;

	[SerializeField]
	float forceInZ = 50f;

	Rigidbody rigidBody;

	// Start is called before the first frame update
	void Start()
	{
		rigidBody = GetComponent<Rigidbody>();
		flag = true;
	}

	// Update is called once per frame
	void Update()
	{	

		if (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))
		{

			touchTimeStart = Time.time; // Take the time of first touch on display

			//+++ ATTENTION: Delete this write previously when create the APK file +++// startPos = Input.GetTouch(0).position; // Take position of first touch on dispaly
			startPos = Input.mousePosition; // ATTENTION: use this only to test game on computer

		}

		if ((Input.GetMouseButtonUp(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)) && flag)
		{

			touchTimeFinish = Time.time; // Take the time of last touch on display

			timeInterval = touchTimeFinish - touchTimeStart; // Calculate time of swipe

			//+++ ATTENTION: Delete this write previously when create the APK file +++// endPos = Input.GetTouch(0).position; // Take position of last touch on dispaly
			endPos = Input.mousePosition; // ATTENTION: use this only to test game on computer

			direction = startPos - endPos; // Calculate direction of swipe

			rigidBody.isKinematic = false;
			rigidBody.AddForce(-direction.x * forceInXandY, -direction.y * forceInXandY, forceInZ / (timeInterval + 0.00001f)); // Add force for ball

			flag = false;

			Destroy(gameObject, 3f); // Destroy ball after 3 seconds

		}
	}
}
