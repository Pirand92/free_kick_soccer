﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	public int countBalls; // Counter for number balls generated

	public static GameManager instance;
	public GameObject ballInstance;

	[SerializeField]
	GameObject ball;

	private void Awake()
	{
		if (instance == null)
			instance = this;
	}

	/* Creation of ball */
	public void Spawn() 
	{

		SegnaPuntiScript.instance.countMissed();

		countBalls++;
		ballInstance = Instantiate(ball, ball.transform.position, Quaternion.identity);

	}


    // Start is called before the first frame update
    void Start()
    {
		countBalls = 1;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
