﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortiereScript : MonoBehaviour
{

	Rigidbody rigidBody;

	float mouseStart;
	float mouseEnd;

	bool right;

	float movingFloat;
	float initialPos;

    // Start is called before the first frame update
    void Start()
    {
		
		rigidBody = GetComponent<Rigidbody>();
		initialPos = rigidBody.transform.position.x;

    }

    // Update is called once per frame
    void Update()
    {

		if (Input.GetMouseButtonDown(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began))
		{

			//+++ ATTENTION: Delete this write previously when create the APK file +++// mouseStart = Input.GetTouch(0).position.x; // Take position of first touch on dispaly
			mouseStart = Input.mousePosition.x; // ATTENTION: use this only to test game on computer

		}

		if ((Input.GetMouseButtonUp(0) || (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)) && GameManager.instance.ballInstance != null)
		{

			//+++ ATTENTION: Delete this write previously when create the APK file +++// mouseEnd = Input.GetTouch(0).position.x; // Take position of last touch on dispaly
			mouseEnd = Input.mousePosition.x; // ATTENTION: use this only to test game on computer

			if (mouseStart > mouseEnd)
			{
				right = true;
				movingFloat = mouseStart - mouseEnd;
			}
			else
			{
				right = false;
				movingFloat = mouseEnd - mouseStart;
			}

			Debug.Log("movingFloat = " + movingFloat);

			// Normalize us this value so that the 'GoalKeeper' position is entry the limits
			if (movingFloat > 55)
				movingFloat = 55;

			if (right)
				rigidBody.transform.Translate(Vector3.right * movingFloat * Time.deltaTime);
			else
				rigidBody.transform.Translate(Vector3.right * -movingFloat * Time.deltaTime);

			Invoke("resetPosition", 6.5f);

		}

	}
	public void resetPosition() {	rigidBody.transform.position = new Vector3(initialPos, transform.position.y, transform.position.z);	}

}
