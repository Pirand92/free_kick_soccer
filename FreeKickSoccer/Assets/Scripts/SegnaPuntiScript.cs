﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SegnaPuntiScript : MonoBehaviour
{

	public static SegnaPuntiScript instance;

	private int goalDone; // Counter for goals done
	private int goalMissed; // Counter for goals missed

	public Text goalDoneTxt; // Show on display number of goals done
	public Text goalMissedTxt; // Show on display number of goals missed

	public GameObject goalText; // Show on display the text "Goal!"
	public AudioClip crowdMP3; // Audio of crowd

	private void Awake()
	{
		if (instance == null)
			instance = this;
	}

	// Start is called before the first frame update
	void Start()
    {
		goalDone = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

	private void OnTriggerEnter(Collider other)
	{

		if (other.gameObject.tag == "Ball")
		{

			goalDone++;
			AudioManagerScript.instance.PlaySound(crowdMP3);
			countMissed();

			goalText.SetActive(true);
			Invoke("disableGoalText", 2f);

			

		}

	}

	public void disableGoalText() { goalText.SetActive(false); }

	public void countMissed()
	{

		goalDoneTxt.text = "Goal: " + goalDone;

		goalMissed = GameManager.instance.countBalls - goalDone;
		goalMissedTxt.text = "Missed: " + goalMissed;

	}

}
